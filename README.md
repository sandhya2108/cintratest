# Cintra DevOps Technical Test

## Introduction
This technical test is part of an application to Cintra Payroll & HR for a position of DevOps Engineer.

## Instructions
1. Clone/download this repository
2. Complete the Tasks listed below
3. Overwrite/edit this `README.md` file with your answers to the Questions listed below
4. Push your work to a public repository and send us the link

## Expectations
- The test has been designed to be worked on for 2-4 hours
- It is likely that you won't be able to finish all of the tasks in the allotted time. Don't worry about this, just prioritise the tasks that you think are most important and relevant to the role
- Do not feel compelled to create a working, deployed application and infrastructure, especially if this comes at the expense of showing your breadth of knowledge

**Important note:** We do not expect candidates to be familiar with all of the technologies mentioned. It is more important that you have an aptitude for learning the types of technology and tools used in DevOps.

---

## Tasks
Please note, the `Suggested technologies` list is not exhaustive. You may use any tool that you are comfortable with and that you think is relevant to the task.

### 1: Docker and CI/CD
Write a CI/CD pipeline to build and deploy the .NET application stored in `sample-application`.

Suggested technologies:

- CircleCI
- Gitlab
- AWS CodeBuild/Deploy

### 2: Infrastructure
Using Infrastructure as Code, write some resources to deploy a basic web application to any cloud platform.

Suggested technologies:

- Terraform
- Saltstack
- CloudFormation

### 3: End-to-end
Extend your CI/CD pipeline to first build and deploy your infrastructure, then build and deploy the application to this infrastructure.

---

## Questions

### 1: How long did you spend on this test? Why that length of time?
Ans : I have almost spent 3 hours on given three task , as i am more comfortable on Infrastructure as Code so initially i started with second task and created YML file and executed it and successfully created an application.
Then i started on understanding the requirements of CI/CD task and found few files are missing wanted to work on it but because of time crunch i could not work on it.Also i had uploaded application code to S3 bucket acting as my source repository to work on code build or code deploy.

### 2: Which technologies did you choose to use and why?
Ans: Task 2. I have used cloudformation to deploy an Appache application with "Hello World" 
I am very much handy with cloudforamtion and creating Yaml file so i have choosed this technology 
-To create CI/CD pipeline to build and deploy application i used AWS codebuild /Deploy

### 3: If you had more time, what would you do differently?
Ans: I could have created buildspec.yml file and dockerfile for docker and CI/CD application also i was facing issues while building sample application so i would require more time to debug that issue.
I very much comfortable with AWS Codebuild and code deploy. I created EC2 instance and tried to create code deploy and run deployment script but it was taking lot of time and it was in pending state so i had to cancel it so that i can look into that issue.
i was pretty much sure that i could crack it but their was time limit so i cancelled that task.
I don't giveup so easliy so if i can get more time i will definetly complete all the given tasks 

### 4: What did you enjoy most about this exercise?
Ans: All the three tasks were very much challenging and instresting to work on it , i liked the most was Infrastructure as code as its my flavor of skill , CI/CD is also my skill but it takes time to compelete the task successfully 

### 5: What did you enjoy least about this exercise?
Ans: Time was major concern in this test, as continuously i had to check time so that i don't cross deadlines , and managed to attempt all the task but was successfull to complete only one task.

### 6: What is the most useful DevOps-related technology that you've used recently, and why?
Ans : I have used Docker Container for deploying an application made few changes in container and done commit and uploaded back to dockerhub and made available for every team member of our project to use it for configuring server .
